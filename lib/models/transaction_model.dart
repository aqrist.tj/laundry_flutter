// To parse required this JSON data, do
//
//     final transactionModel = transactionModelFromJson(jsonString);

import 'dart:convert';

TransactionModel transactionModelFromJson(String str) =>
    TransactionModel.fromJson(json.decode(str));

String transactionModelToJson(TransactionModel data) =>
    json.encode(data.toJson());

class TransactionModel {
  TransactionModel({
    required this.transaction,
  });

  final List<Transaction> transaction;

  factory TransactionModel.fromJson(Map<String, dynamic> json) =>
      TransactionModel(
        transaction: List<Transaction>.from(
            json["transaction"].map((x) => Transaction.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "transaction": List<dynamic>.from(transaction.map((x) => x.toJson())),
      };
}

class Transaction {
  Transaction({
    required this.id,
    required this.userId,
    required this.nameCustomer,
    required this.phone,
    required this.address,
    required this.weight,
    required this.paket,
    required this.total,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
    required this.user,
  });

  final int id;
  final int userId;
  final String nameCustomer;
  final String phone;
  final String address;
  final int weight;
  final String paket;
  final int total;
  final String status;
  final DateTime createdAt;
  final DateTime updatedAt;
  final User user;

  factory Transaction.fromJson(Map<String, dynamic> json) => Transaction(
        id: json["id"],
        userId: json["user_id"],
        nameCustomer: json["nameCustomer"],
        phone: json["phone"],
        address: json["address"],
        weight: json["weight"],
        paket: json["paket"],
        total: json["total"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        user: User.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "nameCustomer": nameCustomer,
        "phone": phone,
        "address": address,
        "weight": weight,
        "paket": paket,
        "total": total,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "user": user.toJson(),
      };
}

class User {
  User({
    required this.id,
    required this.name,
  });

  final int id;
  final String name;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
      };
}
