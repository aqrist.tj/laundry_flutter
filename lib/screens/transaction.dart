import 'dart:convert';

import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:toko_sembako/constant.dart';
import 'package:toko_sembako/models/transaction_model.dart';

import '../models/api_response.dart';
import '../models/trans.dart';
import '../services/trans_service.dart';
import '../services/user_service.dart';
import 'login.dart';
import 'package:http/http.dart' as http;

class Transaction extends StatefulWidget {
  const Transaction({super.key});

  @override
  State<Transaction> createState() => _TransactionState();
}

class _TransactionState extends State<Transaction> {
  List<dynamic> _transactionList = [];
  TransactionModel? transactionModel;
  int userId = 0;
  bool _loading = false;

  //get all transaction
  Future<void> retrieveTransactions() async {
    userId = await getUserId();
    ApiResponse response = await getTransaction();

    if (response.error == null) {
      setState(() {
        _transactionList = response.data as List<dynamic>;
        _loading = _loading ? !_loading : _loading;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => Login()),
                (route) => false)
          });
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('${response.error}'),
      ));
    }
  }

  // get list
  getList() async {
    String token = await getToken();
    var response = await http.get(Uri.parse(storeTransaction), headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    });
    if (response.statusCode == 200) {
      setState(() {
        _loading = false;
      });
      transactionModel = TransactionModel.fromJson(json.decode(response.body));
    } else {
      throw Exception(
        'Request failed with status: ${response.statusCode}.',
      );
    }
  }

  @override
  void initState() {
    retrieveTransactions();
    getList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _loading
        ? const Center(
            child: CircularProgressIndicator(),
          )
        // : ListView.builder(
        //     itemCount: _transactionList.length,
        //     itemBuilder: (BuildContext context, int index) {
        //       Trans trans = _transactionList[index];
        //       return Text('${trans.nameCustomer}');
        //     },
        //   );
        : ListView(
            children: transactionModel?.transaction.map((item) {
                  return Text(item.nameCustomer);
                }).toList() ??
                [
                  const Text('No data'),
                ],
          );
  }
}
